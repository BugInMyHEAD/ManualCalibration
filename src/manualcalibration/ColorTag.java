/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manualcalibration;

/**
 * @author BugInMyHEAD
 */
public class ColorTag {
  public final Color color;
  public final String tag;

  public ColorTag(Color color, String tag) {
    this.color = color;
    this.tag = tag;
  }

  @Override
  public String toString() {
    return tag;
  }
}
