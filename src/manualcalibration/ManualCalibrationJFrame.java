/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package manualcalibration;

/**
 * @author BugInMyHEAD
 */
public class ManualCalibrationJFrame extends JFrame {
  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    JFrame jframe = new ManualCalibrationJFrame("Color Adjustment Helper");
    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jframe.setVisible(true);
  }

  JPanel northJPanel = new JPanel(new GridBagLayout());
  JPanel centerJPanel = new JPanel(new GridLayout(1, 0));

  ColorTag[] colorTags =
      {
          new ColorTag(Color.white, "White"),
          new ColorTag(Color.red, "Red"),
          new ColorTag(Color.green, "Green"),
          new ColorTag(Color.blue, "Blue")
      };
  JComboBox<ColorTag> jcombobox = new JComboBox<>(colorTags);
  JTextField diffJTextField = new JTextField(4);
  JScrollBar colorJScrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
  JScrollBar diffJScrollBar = new JScrollBar();

  JPanel leftJPanel = new JPanel(new BorderLayout());
  JPanel rightJPanel = new JPanel(new BorderLayout());

  JLabel leftJLabel = new JLabel();
  JLabel rightJLabel = new JLabel();

  {
    setSize(900, 600);

    add(northJPanel, BorderLayout.NORTH);
    add(centerJPanel, BorderLayout.CENTER);

    GridBagConstraints northGridBagConstraints = new GridBagConstraints();
    northGridBagConstraints.fill = GridBagConstraints.BOTH;
    northGridBagConstraints.weightx = 0;
    northGridBagConstraints.gridx = 0;
    northJPanel.add(jcombobox, northGridBagConstraints);
    northGridBagConstraints.gridx = 1;
    northJPanel.add(new JSeparator(JSeparator.VERTICAL));
    northGridBagConstraints.weightx = 1;
    northGridBagConstraints.gridx = 2;
    northJPanel.add(colorJScrollBar, northGridBagConstraints);
    northGridBagConstraints.gridx = 3;
    northGridBagConstraints.weightx = 0;
    northJPanel.add(new JSeparator(JSeparator.VERTICAL));
    northGridBagConstraints.gridx = 4;
    northJPanel.add(diffJTextField, northGridBagConstraints);
    northGridBagConstraints.gridx = 5;
    northJPanel.add(diffJScrollBar, northGridBagConstraints);

    centerJPanel.add(leftJPanel);
    centerJPanel.add(rightJPanel);

    diffJScrollBar.setMinimumSize(new Dimension(0, 0));
    diffJScrollBar.setPreferredSize(diffJScrollBar.getMinimumSize());
    diffJScrollBar.setUnitIncrement(-1);

    leftJPanel.add(leftJLabel, BorderLayout.SOUTH);
    rightJPanel.add(rightJLabel, BorderLayout.SOUTH);

    leftJLabel.setOpaque(true);
    rightJLabel.setOpaque(true);
    leftJLabel.setHorizontalAlignment(SwingConstants.CENTER);
    rightJLabel.setHorizontalAlignment(SwingConstants.CENTER);

    diffJScrollBar.addAdjustmentListener(ev2 ->
    {
      int value = colorJScrollBar.getValue();
      int diff = diffJScrollBar.getValue();

      if (value + diff < 0) {
        value = -diff;
      } else if (value + diff > 0xFF) {
        value = 0xFF - diff;
      }
      if (diff > 0) {
        colorJScrollBar.setValues(value, 0, 0, 0xFF - diff);
      } else {
        colorJScrollBar.setValues(value, 0, 0 - diff, 0xFF);
      }

      try {
        diffJTextField.setText(Integer.toString(diff));
      } catch (IllegalStateException exc2) {
      }
    });

    colorJScrollBar.addAdjustmentListener(ev2 ->
    {
      colorJScrollBar.grabFocus();

      int leftValue = colorJScrollBar.getValue();
      int rightValue = leftValue + diffJScrollBar.getValue();

      switch (jcombobox.getSelectedIndex()) {
        case 0:
          leftJPanel.setBackground(new Color(leftValue, leftValue, leftValue));
          rightJPanel.setBackground(new Color(rightValue, rightValue, rightValue));
          break;
        case 1:
          leftJPanel.setBackground(new Color(leftValue, 0, 0));
          rightJPanel.setBackground(new Color(rightValue, 0, 0));
          break;
        case 2:
          leftJPanel.setBackground(new Color(0, leftValue, 0));
          rightJPanel.setBackground(new Color(0, rightValue, 0));
          break;
        case 3:
          leftJPanel.setBackground(new Color(0, 0, leftValue));
          rightJPanel.setBackground(new Color(0, 0, rightValue));
          break;
      }

      leftJLabel.setText(Integer.toString(leftValue));
      rightJLabel.setText(Integer.toString(rightValue));
    });

    jcombobox.addItemListener(ev2 ->
    {
      if (ItemEvent.SELECTED == ev2.getStateChange()) for (AdjustmentListener adjLis2 : colorJScrollBar.getAdjustmentListeners()) {
        adjLis2.adjustmentValueChanged(null);
      }
    });

    diffJTextField.addCaretListener(ev2 ->
    {
      try {
        int i4 = Integer.parseInt(diffJTextField.getText());
        if (i4 < -0xFF || 0xFF < i4) {
          throw new NumberFormatException();
        }
        diffJScrollBar.setValues(i4, 0, -0xFF, 0xFF);
        diffJTextField.grabFocus();
      } catch (NumberFormatException exc2) {
      }
    });
  }

  public ManualCalibrationJFrame() {
    this(null);
  }

  public ManualCalibrationJFrame(String title) {
    super(title);

    diffJScrollBar.setValues(5, 0, -0xFF, 0xFF);
  }
}
